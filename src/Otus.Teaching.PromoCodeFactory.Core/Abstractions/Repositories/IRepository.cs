﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Threading.Tasks;
using Otus.Teaching.PromoCodeFactory.Core.Domain;

namespace Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories
{
    public interface IRepository<T>
        where T: BaseEntity
    {
        Task<IEnumerable<T>> GetAllAsync();
        
        Task<T> GetByIdAsync(Guid id);

        Task<IEnumerable<T>> GetByWhereAsync(Expression<Func<T, bool>> where, params Expression<Func<T, object>>[] includeProperties);

        Task<T> AddAsync(T newEntity);

        Task<T> UpdateAsync(T updatedEntity);

        Task DeleteAsync(Guid id);

        Task DeleteRangeAsync(Func<T, bool> where);
    }
}