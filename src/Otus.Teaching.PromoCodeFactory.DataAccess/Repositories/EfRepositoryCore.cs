﻿using Microsoft.EntityFrameworkCore;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace Otus.Teaching.PromoCodeFactory.DataAccess.Repositories
{
    public abstract class EfRepositoryCore<TContext, TEntity> : IDisposable, IRepository<TEntity> where TContext : DbContext where TEntity : BaseEntity
    {
        protected readonly TContext context;
        public EfRepositoryCore(TContext context)
        {
            this.context = context;
        }
        public virtual async Task<TEntity> AddAsync(TEntity newEntity)
        {
            context.Set<TEntity>().Add(newEntity);
            await Commit();
            return newEntity;
        }

        public async Task DeleteAsync(Guid id)
        {
            var entity = await context.Set<TEntity>().FindAsync(id);
            if (entity == null)
                return;

            context.Set<TEntity>().Remove(entity);
            await Commit();

            return;
        }

        public async Task DeleteRangeAsync(Func<TEntity, bool> where)
        {
            var entities = context.Set<TEntity>().Where(where);
            context.Set<TEntity>().RemoveRange(entities);
            await Commit();
        }

        public async Task<IEnumerable<TEntity>> GetAllAsync()
        {
            return await context.Set<TEntity>().ToListAsync();
        }

        public virtual async Task<TEntity> GetByIdAsync(Guid id)
        {
            return await context.Set<TEntity>().FindAsync(id);
        }

        public virtual Task<IEnumerable<TEntity>> GetByWhereAsync(Expression<Func<TEntity, bool>> where, params Expression<Func<TEntity, object>>[] includeProperties)
        {
            IQueryable<TEntity> query = context.Set<TEntity>();
            query = query.Where(where);
            if (includeProperties != null && includeProperties.Count() > 0)
            {
                foreach (var includeProperty in includeProperties)
                {
                    query = query.Include(includeProperty);
                }
            }
                        
            return Task.FromResult(query.AsEnumerable());
        }        

        public virtual async Task<TEntity> UpdateAsync(TEntity updatedEntity)
        {
            context.Entry(updatedEntity).State = EntityState.Modified;
            await Commit();
            return updatedEntity;
        }

        protected async Task Commit()
        {
            await context.SaveChangesAsync();
        }

        private bool disposed = false;

        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    context.Dispose();
                }
            }
            this.disposed = true;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
}
