﻿using Microsoft.EntityFrameworkCore;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.DataAccess.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Otus.Teaching.PromoCodeFactory.DataAccess.Repositories
{
    public class EfRepositoryCustomer: EfRepositoryCore<DataContext, Customer>
    {
        public EfRepositoryCustomer(DataContext context): base(context)
        {

        }

        public override async Task<Customer> GetByIdAsync(Guid id)
        {
            return await context.Set<Customer>()
                .Include(c => c.Preferences).ThenInclude(p => p.Preference)
                .Include(c => c.PromoCodes)
                .AsNoTracking()
                .FirstOrDefaultAsync(p=>p.Id == id);
        }

        public override async Task<Customer> AddAsync(Customer newEntity)
        {
            var preferences = newEntity.Preferences;
            newEntity.Preferences = null;
            var customer = await base.AddAsync(newEntity);
            customer = await UpdatePreferences(preferences, customer);
            return customer;
        }

        public override async Task<Customer> UpdateAsync(Customer updatedEntity)
        {
            var updatedPreferences = updatedEntity.Preferences;
            updatedEntity.Preferences = null;
            var customer = await base.UpdateAsync(updatedEntity);
            customer = await UpdatePreferences(updatedPreferences, customer);
            return customer;
        }

        private async Task<Customer> UpdatePreferences(ICollection<CustomerPreference> updatedPreferences, Customer customer)
        {
            if (updatedPreferences != null && updatedPreferences.Count > 0)
            {
                var customerPreferenceByCustomer = await context.Set<CustomerPreference>().Where(p => p.CustomerId == customer.Id).ToListAsync();
                foreach (var item in updatedPreferences)
                {
                    var existCustomerPreference = customerPreferenceByCustomer.FirstOrDefault(p => p.CustomerId == customer.Id && p.PreferenceId == item.PreferenceId);
                    if (existCustomerPreference == null)
                        await context.Set<CustomerPreference>().AddAsync(new CustomerPreference() { CustomerId = customer.Id, PreferenceId = item.PreferenceId });
                    else
                        customerPreferenceByCustomer.Remove(existCustomerPreference);
                }
                if(customerPreferenceByCustomer.Count() > 0)
                    context.Set<CustomerPreference>().RemoveRange(customerPreferenceByCustomer);
                await Commit();
                return await GetByIdAsync(customer.Id);
            }
            return customer;
        }
    }
}
