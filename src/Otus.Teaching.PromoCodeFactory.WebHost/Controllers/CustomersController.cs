﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;
using Otus.Teaching.PromoCodeFactory.WebHost.Services;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Controllers
{
    /// <summary>
    /// Клиенты
    /// </summary>
    [ApiController]
    [Route("api/v1/[controller]")]
    public class CustomersController
        : ControllerBase
    {
        private readonly IRepository<Customer> _customerRepository;
        private readonly IRepository<PromoCode> _promocodeRepository;

        public CustomersController(IRepository<Customer> customerRepository, IRepository<PromoCode> promocodeRepository)
        {
            _customerRepository = customerRepository;
            _promocodeRepository = promocodeRepository;
        }

        /// <summary>
        /// Получение всех клиентов
        /// </summary>
        /// <returns>Список клиентов</returns>
        [HttpGet]
        public async Task<IEnumerable<CustomerShortResponse>> GetCustomersAsync()
        {
            var customers = await _customerRepository.GetAllAsync();

            var customersModelList = customers.Select(x=>
            new CustomerShortResponse()
            {
                Id = x.Id,
                Email = x.Email,
                FirstName = x.FirstName,
                LastName = x.LastName
            });

            return customersModelList;
        }
        
        /// <summary>
        /// Получение подробной информации по клиенту
        /// </summary>
        /// <param name="id">Идентификатор клиента</param>
        /// <returns>Информация по клиенту</returns>
        [HttpGet("{id}")]
        public async Task<ActionResult<CustomerResponse>> GetCustomerAsync(Guid id)
        {
            var customer = await _customerRepository.GetByIdAsync(id);

            if (customer == null)
                return NotFound();

            var customerModel = new CustomerResponse()
            {
                Id = customer.Id,
                Email = customer.Email,
                FirstName = customer.FirstName,
                LastName = customer.LastName,
                PromoCodes = customer.PromoCodes?.Select(p => new PromoCodeShortResponse() { 
                    BeginDate = p.BeginDate.FormatDate(),
                    Code = p.Code,
                    EndDate = p.EndDate.FormatDate(),
                    Id = p.Id,
                    PartnerName = p.PartnerName,
                    ServiceInfo = p.ServiceInfo
                }),
                Preferences = customer.Preferences?.Select(p=> new PreferenceShortResponse()
                {
                    Name = p.Preference.Name
                })
            };

            return customerModel;
        }
        
        /// <summary>
        /// Добавление нового клиента
        /// </summary>
        /// <param name="request">Информация о новом клиенте</param>
        /// <returns>Результат операции</returns>
        [HttpPost]
        public async Task<IActionResult> CreateCustomerAsync(CreateOrEditCustomerRequest request)
        {
            //TODO: Добавить создание нового клиента вместе с его предпочтениями
            Customer customer = await _customerRepository.AddAsync(new Customer()
            {
                Email = request.Email,
                FirstName = request.FirstName,
                LastName = request.LastName,
                Preferences = request.PreferenceIds.Select(p => new CustomerPreference() { PreferenceId = p }).ToList()
            });
            return Ok();
        }
        
        /// <summary>
        /// Обновление информации о клиенте
        /// </summary>
        /// <param name="id">Идентификатор клиента</param>
        /// <param name="request">Обновлённые данные по клиенту</param>
        /// <returns></returns>
        [HttpPut("{id}")]
        public async Task<IActionResult> EditCustomersAsync(Guid id, CreateOrEditCustomerRequest request)
        {
            Customer customer = await _customerRepository.UpdateAsync(new Customer()
            {
                Id = id,
                Email = request.Email,
                FirstName = request.FirstName,
                LastName = request.LastName,
                Preferences = request.PreferenceIds.Select(p => new CustomerPreference() { PreferenceId = p }).ToList()
            });
            return Ok();
        }
        
        /// <summary>
        /// Удаление информации по клиенту вместе с промокодами
        /// </summary>
        /// <param name="id">Идентификатор клиента</param>
        /// <returns></returns>
        [HttpDelete]
        public async Task<IActionResult> DeleteCustomer(Guid id)
        {
            await _promocodeRepository.DeleteRangeAsync(p => p.PartnerId == id);
            await _customerRepository.DeleteAsync(id);
            return Ok();
        }        
    }
}