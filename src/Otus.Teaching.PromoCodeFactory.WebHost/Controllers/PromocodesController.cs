﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Namotion.Reflection;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;
using Otus.Teaching.PromoCodeFactory.WebHost.Services;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Controllers
{
    /// <summary>
    /// Промокоды
    /// </summary>
    [ApiController]
    [Route("api/v1/[controller]")]
    public class PromocodesController
        : ControllerBase
    {
        IRepository<PromoCode> _promoCodeRepository;
        IRepository<Customer> _customerRepository;

        public PromocodesController(IRepository<PromoCode> promoCodeRepository, IRepository<Customer> customerRepository)
        {
            _promoCodeRepository = promoCodeRepository;
            _customerRepository = customerRepository;
        }

        /// <summary>
        /// Получить все промокоды
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<IEnumerable<PromoCodeShortResponse>> GetPromocodesAsync()
        {
            var promoCodes = await _promoCodeRepository.GetAllAsync();

            var promoCodesModelList = promoCodes.Select(p => new PromoCodeShortResponse()
            {
                BeginDate = p.BeginDate.FormatDate(),
                Code = p.Code,
                EndDate = p.EndDate.FormatDate(),
                Id = p.Id,
                PartnerName = p.PartnerName,
                ServiceInfo = p.ServiceInfo
            });

            return promoCodesModelList;
        }
        
        /// <summary>
        /// Создать промокод и выдать его клиентам с указанным предпочтением
        /// </summary>
        /// <returns>Результат выполнения операции</returns>
        [HttpPost]
        public async Task<IActionResult> GivePromoCodesToCustomersWithPreferenceAsync(GivePromoCodeRequest request)
        {
            var customersForGivePromocode = await _customerRepository.GetByWhereAsync(p=>p.Preferences.Any(pref=>pref.PreferenceId == request.Preference), c=>c.Preferences);
            foreach(var customerForGive in customersForGivePromocode)
            {
                await _promoCodeRepository.AddAsync(new PromoCode()
                {
                    PartnerId = customerForGive.Id,
                    ServiceInfo = request.ServiceInfo,
                    PartnerName = request.PartnerName,
                    Code = request.PromoCode,
                    PreferenceId = request.Preference,
                    BeginDate = request.BeginDate.ParseDate(),
                    EndDate = request.EndDate.ParseDate()
                });
            }

            return Ok();
        }
    }
}