﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Models
{
    /// <summary>
    /// Выдача промокода всем клиентам
    /// </summary>
    public class GivePromoCodeRequest
    {
        /// <summary>
        /// Информация по промокоду
        /// </summary>        
        public string ServiceInfo { get; set; }
        /// <summary>
        /// Имя партнёра
        /// </summary>
        [Required]
        public string PartnerName { get; set; }
        /// <summary>
        /// Название промокода
        /// </summary>
        [Required]
        public string PromoCode { get; set; }
        /// <summary>
        /// Ид предпочтения для которого выдаётся промокод
        /// </summary>
        [Required]
        public Guid Preference { get; set; }
        /// <summary>
        /// Дата начала действия промокода
        /// </summary>
        [Required]
        public string BeginDate { get; set; }
        /// <summary>
        /// Дата окончания действия промокода
        /// </summary>
        [Required]
        public string EndDate { get; set; }
    }
}