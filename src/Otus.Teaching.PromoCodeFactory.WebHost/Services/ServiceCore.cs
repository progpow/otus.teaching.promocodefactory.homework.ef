﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Services
{
    public static class ServiceCore
    {
        private const string dateFormat = "yyyy-MM-dd";
        public static string FormatDate(this DateTime formattingValue)
        {
            return formattingValue.ToString(dateFormat);
        }

        public static DateTime ParseDate(this string date)
        {
            return DateTime.ParseExact(date, dateFormat, null);
        }
    }
}
