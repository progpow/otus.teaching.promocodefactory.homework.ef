﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Settings
{
    public class CommonOptions
    {
        public const string Common = "Common";

        public bool TraceQueriesToConsole { get; set; }
        public bool ResetDbOnStart { get; set; }
    }
}
