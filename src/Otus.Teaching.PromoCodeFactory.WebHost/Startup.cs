using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Internal;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.Administration;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.DataAccess.Data;
using Otus.Teaching.PromoCodeFactory.DataAccess.Repositories;
using Otus.Teaching.PromoCodeFactory.WebHost.Settings;

namespace Otus.Teaching.PromoCodeFactory.WebHost
{
    public class Startup
    {
        const string configConnectionString = "sqlLiteConnectionString";

        const string openApiTitle = "PromoCode Factory API Doc";
        const string openApiVersion = "1.0";

        IConfiguration Configuration{ get; }
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        // This method gets called by the runtime. Use this method to add services to the container.
        // For more information on how to configure your application, visit https://go.microsoft.com/fwlink/?LinkID=398940
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddControllers();
            services.AddDbContext<DataContext>(p =>
            {   
                p.UseSqlite(Configuration.GetConnectionString(configConnectionString));
                
                if(GetCommonOptions().TraceQueriesToConsole)
                    p.UseLoggerFactory(LoggerFactory.Create(builder => { builder.AddConsole(); }));
            });
            services.AddScoped(typeof(IRepository<Employee>), typeof(EfRepository<Employee>));
            services.AddScoped(typeof(IRepository<Role>), typeof(EfRepository<Role>));
            services.AddScoped(typeof(IRepository<Preference>), typeof(EfRepository<Preference>));
            services.AddScoped(typeof(IRepository<Customer>), typeof(EfRepositoryCustomer));
            services.AddScoped(typeof(IRepository<PromoCode>), typeof(EfRepository<PromoCode>));

            services.AddOpenApiDocument(options =>
            {
                options.Title = openApiTitle;
                options.Version = openApiVersion;
            });
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env, DataContext dataContext)
        {
            if (GetCommonOptions().ResetDbOnStart)
                dataContext.Database.EnsureDeleted();                
            
            dataContext.Database.EnsureCreated();

            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseHsts();
            }

            app.UseOpenApi();
            app.UseSwaggerUi3(x =>
            {
                x.DocExpansion = "list";
            });
            
            app.UseHttpsRedirection();

            app.UseRouting();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });
        }

        private CommonOptions GetCommonOptions()
        {
            var commonOptions = new CommonOptions();
            Configuration.GetSection(CommonOptions.Common).Bind(commonOptions);
            return commonOptions;
        }
    }
}